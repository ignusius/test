package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"bitbucket.org/ignusius/simpledb"
	"github.com/abbot/go-http-auth"
	_ "github.com/mattn/go-sqlite3"
)

type page struct {
	Table  [][]interface{}
	Status bool
}

var (
	templateFile string
	db           *simpledb.DB
	statusPost   bool
)

type configuration struct {
	User     string
	Password string
	Port     string
}

func getConfig() configuration {
	file, err := os.Open("conf.json")
	if err != nil {
		panic("Config file not found!")
	}
	decoder := json.NewDecoder(file)
	configuration := configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("error:", err)
	}
	return configuration
}

//Secret  - user and pass for http
func Secret(user, realm string) string {
	if user == getConfig().User {

		return getConfig().Password
	}
	return ""
}

func handlerICon(w http.ResponseWriter, r *http.Request) {}

func mainPage(route string, Filetemplate string) {

	templateFile = Filetemplate
	authenticator := auth.NewBasicAuthenticator(":", Secret)

	http.HandleFunc(route, authenticator.Wrap(func(w http.ResponseWriter, r *auth.AuthenticatedRequest) {
		w.Header().Set("Content-type", "text/html")

		t, err := template.ParseFiles(templateFile)
		if err != nil {
			panic("Template : file not exist!")
		}

		pwd, err := os.Getwd()
		if err != nil {
			panic("Getwd Error!")
		}

		dst, err := os.Create(filepath.Join(pwd+"/static/share/", "template.csv"))
		if err != nil {
			panic("os.Create Error!")
		}

		tmpate, err := os.Open(filepath.Join(pwd+"/static/share/", "template.csv"))
		if err != nil {
			panic("/static/share/template.csv not found!")
		}

		statusPost = false
		frmCategory := r.PostFormValue("password")
		fmt.Println(frmCategory)

		if r.Method == "POST" {

			db.Exec("DELETE FROM DATA")
			src, _, err := r.FormFile("my-file")
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			defer src.Close()

			defer dst.Close()

			io.Copy(dst, src)

			reader := csv.NewReader(bufio.NewReader(tmpate))
			reader.Comma = '|'
			reader.LazyQuotes = true

			db.Begin()
			db.TxPrepare("INSERT INTO data values ($1,$2,$3,$4,$5)")
			for {
				line, error := reader.Read()
				if error == io.EOF {
					break
				} else if error != nil {
					log.Fatal(error)
				}

				if line[0] == "" || line[0] == " " {
					fmt.Fprint(w, `<html><meta charset='utf-8'><h1>Ошибка</h1>
						<p>Артикул пуст (наименование: `+line[1]+`)</p><a href="/">Назад</a></html>`)
					db.Rollback()
					return

				}

				if len(line) != 5 {
					fmt.Fprint(w, `<html><meta charset='utf-8'><h1>Ошибка</h1><p>Неверный
						формат данных</p><a href="/">Назад</a></html>`)

					return
				}

				db.StmtExec(line[0], line[1], line[2], line[3], line[4])

			}
			db.Commit()
			check, err := db.Query("SELECT article FROM data GROUP BY 1 HAVING COUNT(*) > 1")
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			if len(check) > 0 {
				fmt.Fprint(w, `<html><meta charset='utf-8'><h1>Ошибка</h1>
					<p>В базе найдены одинаковые артикулы</p></p><a href="/">Назад</a></html>`)
				return
			}

			statusPost = true

		}
		arrcsv, err := db.Query("SELECT * FROM DATA")

		t.Execute(w, &page{Table: arrcsv, Status: statusPost})
	}))
}

func static(staticDirs []string) {
	for _, item := range staticDirs {
		http.Handle(item, http.StripPrefix(item, http.FileServer(http.Dir("."+item))))
	}
}

//Run is starting http server
func run(address, port string) {
	log.Println("Listening...")
	http.ListenAndServe(address+":"+port, nil)
}

func main() {

	db = new(simpledb.DB)
	// or db:=simpledb.DB{}
	err := db.NewDatabase("sqlite3", "static/share/template.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	static([]string{"/static/share/", "/static/css/", "/static/img/"})
	mainPage("/", "index.html")
	http.HandleFunc("/favicon.ico", handlerICon)
	run("", getConfig().Port)

}
